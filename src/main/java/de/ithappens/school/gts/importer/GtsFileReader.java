package de.ithappens.school.gts.importer;

import org.apache.commons.lang3.StringUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

/**
 * Copyright: 2020
 * Organization: IT-Happens.de
 * @author Ryppo
 */
public class GtsFileReader {

    /**
     * Print content of the GTS file from URL to standard output
     * @param fileUrl URL of the GTS file
     * @throws IOException
     * @throws ParserConfigurationException
     * @throws SAXException
     * @throws ParseException
     */
    public static void printGtsFile(URL fileUrl) throws IOException, ParserConfigurationException, SAXException, ParseException {
        System.out.println("Print GTS file of URL '" + (fileUrl != null ? fileUrl.toString() : "<empty>"));
        if (fileUrl != null) {
            InputStream in = fileUrl.openStream();
            Document doc = parse(in);
            doc.getDocumentElement().normalize();
            Element element = doc.getDocumentElement();
            NodeList submissionCandidates = element != null ? element.getChildNodes() : null;
            int submissionCandidatesCount = submissionCandidates != null ? submissionCandidates.getLength() : 0;
            System.out.println("+ Count submission candidates: " + submissionCandidatesCount);
            int countSubmissions = 0;
            if (submissionCandidatesCount > 0) {
                for (int candidatePos = 0; candidatePos < submissionCandidatesCount; candidatePos++) {
                    Node submissionCandidate = submissionCandidates.item(candidatePos);
                    if (submissionCandidate != null && submissionCandidate.getNodeName().equals(XML_TAG__SUBMISSION)) {
                        countSubmissions++;
                        GtsSubmission model = parse(submissionCandidate);
                        System.out.println(model.identify());
                    }
                }
            } else {
                System.out.println("No ");
            }
            System.out.println("+ Count submissions: " + countSubmissions);
        }
    }

    /**
     * Parse the provided XML node into GtsSubmission object
     * @param submission XML node with GtsSubmission content
     * @return instance of GtsSubmission with content of the provided XML node
     * @throws ParseException
     */
    private static GtsSubmission parse(Node submission) throws ParseException {
        GtsSubmission model = null;
        if (submission != null) {
            model = new GtsSubmission();
            model.setCreationDate(parseCreationDate(submission));
            NodeList fields = submission.getChildNodes();
            int fieldCount = fields.getLength();
            for (int fieldNo = 0; fieldNo < fieldCount; fieldNo++) {
                Node field = fields.item(fieldNo);
                String nodeName = field.getNodeName();
                if (nodeName.equals(XML_FIELD)) {
                    String fieldType = field.getAttributes().getNamedItem(XML_FIELD_NAME).getNodeValue();
                    Node fieldValueNode = field.getFirstChild();
                    String fieldValue = null;
                    if (fieldValueNode != null) {
                        fieldValue = field.getFirstChild().getNodeValue();
                    }
                    fillModelForField(fieldType, fieldValue, model);
                }
            }
        }
        return model;
    }

    private final static String GTS2_SEPARATOR = " ";

    private static String parseValueForGts2(String rawValue) {
        return StringUtils.substringBeforeLast(rawValue, GTS2_SEPARATOR);
    }

    private static BigDecimal parseCostsInEuroForGts2(String rawValue) {
        String costsWithEuroString = StringUtils.substringAfterLast(rawValue, GTS2_SEPARATOR);
        String costsWithEuroAndDecimalPointString = StringUtils.replace(costsWithEuroString,",",".");
        String costsNativeString = StringUtils.remove(costsWithEuroAndDecimalPointString,"€");
        return StringUtils.isNotEmpty(costsNativeString) ? new BigDecimal(costsNativeString) : BigDecimal.ZERO;
    }

    /**
     * Fill the provided GtsSubmission model with the provided field value to the proper field
     * @param fieldType name of field to be filled
     * @param fieldValue value of the field to fill
     * @param model GtsSubmission to set the value
     */
    private static void fillModelForField(String fieldType, String fieldValue, GtsSubmission model) {
        if (model != null && StringUtils.isNotEmpty(fieldType)) {
            switch (fieldType) {
                case XML_FIELD__NAME_DES_KINDES:
                    model.setNameDesKindes(fieldValue);
                    break;
                case XML_FIELD__KLASSE:
                    model.setKlasse(fieldValue);
                    break;
                case XML_FIELD__TAEGLICHE_BETREUUNG:
                    model.setTaeglicheBetreuung(fieldValue);
                    break;
                case XML_FIELD__GTS_1_MONTAG:
                    model.gts1Items.add(new Gts1Item(GtsItem.DAY_OF_WEEK.MONDAY, fieldValue));
                    break;
                case XML_FIELD__GTS_1_DIENSTAG:
                    model.gts1Items.add(new Gts1Item(GtsItem.DAY_OF_WEEK.TUESDAY, fieldValue));
                    break;
                case XML_FIELD__GTS_1_MITTWOCH:
                    model.gts1Items.add(new Gts1Item(GtsItem.DAY_OF_WEEK.WEDNESDAY, fieldValue));
                    break;
                case XML_FIELD__GTS_1_DONNERSTAG:
                    model.gts1Items.add(new Gts1Item(GtsItem.DAY_OF_WEEK.THURSDAY, fieldValue));
                    break;
                case XML_FIELD__GTS_1_FREITAG:
                    model.gts1Items.add(new Gts1Item(GtsItem.DAY_OF_WEEK.FRIDAY, fieldValue));
                    break;
                case XML_FIELD__GTS_2_MONTAG_WUNSCH_1:
                    model.gts2Items.add(new Gts2Item(GtsItem.DAY_OF_WEEK.MONDAY, GtsItem.PRIORITY.ONE, parseValueForGts2(fieldValue), parseCostsInEuroForGts2(fieldValue)));
                    break;
                case XML_FIELD__GTS_2_MONTAG_WUNSCH_2:
                    model.gts2Items.add(new Gts2Item(GtsItem.DAY_OF_WEEK.MONDAY, GtsItem.PRIORITY.TWO, parseValueForGts2(fieldValue), parseCostsInEuroForGts2(fieldValue)));
                    break;
                case XML_FIELD__GTS_2_MONTAG_WUNSCH_3:
                    model.gts2Items.add(new Gts2Item(GtsItem.DAY_OF_WEEK.MONDAY, GtsItem.PRIORITY.THREE, parseValueForGts2(fieldValue), parseCostsInEuroForGts2(fieldValue)));
                    break;
                case XML_FIELD__GTS_2_DIENSTAG_WUNSCH_1:
                    model.gts2Items.add(new Gts2Item(GtsItem.DAY_OF_WEEK.TUESDAY, GtsItem.PRIORITY.ONE, parseValueForGts2(fieldValue), parseCostsInEuroForGts2(fieldValue)));
                    break;
                case XML_FIELD__GTS_2_DIENSTAG_WUNSCH_2:
                    model.gts2Items.add(new Gts2Item(GtsItem.DAY_OF_WEEK.TUESDAY, GtsItem.PRIORITY.TWO, parseValueForGts2(fieldValue), parseCostsInEuroForGts2(fieldValue)));
                    break;
                case XML_FIELD__GTS_2_DIENSTAG_WUNSCH_3:
                    model.gts2Items.add(new Gts2Item(GtsItem.DAY_OF_WEEK.TUESDAY, GtsItem.PRIORITY.THREE, parseValueForGts2(fieldValue), parseCostsInEuroForGts2(fieldValue)));
                    break;
                case XML_FIELD__GTS_2_MITTWOCH_WUNSCH_1:
                    model.gts2Items.add(new Gts2Item(GtsItem.DAY_OF_WEEK.WEDNESDAY, GtsItem.PRIORITY.ONE, parseValueForGts2(fieldValue), parseCostsInEuroForGts2(fieldValue)));
                    break;
                case XML_FIELD__GTS_2_MITTWOCH_WUNSCH_2:
                    model.gts2Items.add(new Gts2Item(GtsItem.DAY_OF_WEEK.WEDNESDAY, GtsItem.PRIORITY.TWO, parseValueForGts2(fieldValue), parseCostsInEuroForGts2(fieldValue)));
                    break;
                case XML_FIELD__GTS_2_MITTWOCH_WUNSCH_3:
                    model.gts2Items.add(new Gts2Item(GtsItem.DAY_OF_WEEK.WEDNESDAY, GtsItem.PRIORITY.THREE, parseValueForGts2(fieldValue), parseCostsInEuroForGts2(fieldValue)));
                    break;
                case XML_FIELD__GTS_2_DONNERSTAG_WUNSCH_1:
                    model.gts2Items.add(new Gts2Item(GtsItem.DAY_OF_WEEK.THURSDAY, GtsItem.PRIORITY.ONE, parseValueForGts2(fieldValue), parseCostsInEuroForGts2(fieldValue)));
                    break;
                case XML_FIELD__GTS_2_DONNERSTAG_WUNSCH_2:
                    model.gts2Items.add(new Gts2Item(GtsItem.DAY_OF_WEEK.THURSDAY, GtsItem.PRIORITY.TWO, parseValueForGts2(fieldValue), parseCostsInEuroForGts2(fieldValue)));
                    break;
                case XML_FIELD__GTS_2_DONNERSTAG_WUNSCH_3:
                    model.gts2Items.add(new Gts2Item(GtsItem.DAY_OF_WEEK.THURSDAY, GtsItem.PRIORITY.THREE, parseValueForGts2(fieldValue), parseCostsInEuroForGts2(fieldValue)));
                    break;
                case XML_FIELD__GTS_2_FREITAG_WUNSCH_1:
                    model.gts2Items.add(new Gts2Item(GtsItem.DAY_OF_WEEK.FRIDAY, GtsItem.PRIORITY.ONE, parseValueForGts2(fieldValue), parseCostsInEuroForGts2(fieldValue)));
                    break;
                case XML_FIELD__GTS_2_FREITAG_WUNSCH_2:
                    model.gts2Items.add(new Gts2Item(GtsItem.DAY_OF_WEEK.FRIDAY, GtsItem.PRIORITY.TWO, parseValueForGts2(fieldValue), parseCostsInEuroForGts2(fieldValue)));
                    break;
                case XML_FIELD__GTS_2_FREITAG_WUNSCH_3:
                    model.gts2Items.add(new Gts2Item(GtsItem.DAY_OF_WEEK.FRIDAY, GtsItem.PRIORITY.THREE, parseValueForGts2(fieldValue), parseCostsInEuroForGts2(fieldValue)));
                    break;
                case XML_FIELD__GTS_3_MONTAG:
                    model.gts3Items.add(new Gts3Item(GtsItem.DAY_OF_WEEK.MONDAY, fieldValue));
                    break;
                case XML_FIELD__GTS_3_DIENSTAG:
                    model.gts3Items.add(new Gts3Item(GtsItem.DAY_OF_WEEK.TUESDAY, fieldValue));
                    break;
                case XML_FIELD__GTS_3_MITTWOCH:
                    model.gts3Items.add(new Gts3Item(GtsItem.DAY_OF_WEEK.WEDNESDAY, fieldValue));
                    break;
                case XML_FIELD__GTS_3_DONNERSTAG:
                    model.gts3Items.add(new Gts3Item(GtsItem.DAY_OF_WEEK.THURSDAY, fieldValue));
                    break;
                case XML_FIELD__GTS_3_FREITAG:
                    model.gts3Items.add(new Gts3Item(GtsItem.DAY_OF_WEEK.FRIDAY, fieldValue));
                    break;
                case XML_FIELD__ANTRAG_AUF_ERMAESSIGUNG:
                    model.setAntragAufErmaessigung(fieldValue);
                    break;
                case XML_FIELD__ERMAESSIGUNGSGRUND_1:
                    model.getErmaessigungsGrund1().setRawValue(fieldValue);
                    break;
                case XML_FIELD__NAME_DES_GESCHWISTERKINDES:
                    model.getErmaessigungsGrund1().setNameDesGeschwisterKindes(fieldValue);
                    break;
                case XML_FIELD__KLASSE_DES_GESCHWISTERKINDES:
                    model.getErmaessigungsGrund1().setKlasseDesGeschwisterKindes(fieldValue);
                    break;
                case XML_FIELD__ERMAESSIGUNGSGRUND_2:
                    model.getErmaessigungsGrund2().setRawValue(fieldValue);
                    break;
                case XML_FIELD__ERMAESSIGUNGSGRUND_3:
                    model.getErmaessigungsGrund3().setRawValue(fieldValue);
                    break;
                case XML_FIELD__BEGRUENDUNG:
                    model.getErmaessigungsGrund3().setBegruendung(fieldValue);
                    break;
                case XML_FIELD__ERZIEHUNGSBERECHTIGTER:
                    model.setErziehungsberechtigter(fieldValue);
                    break;
                case XML_FIELD__ORT:
                    model.setOrt(fieldValue);
                    break;
                case XML_FIELD__TELEFONNUMMER:
                    model.setTelefonnummer(fieldValue);
                    break;
                default:
                    model.unknownAttributes.put(fieldType,fieldValue);
            }
        }
    }

    /**
     * Parse the creation date attribute from the provided xml node with GtsSubmission content
     * @param submission Xml node with GtsSubmission content
     * @return Parsed creation date from the xml node
     * @throws ParseException
     */
    private static Date parseCreationDate(Node submission) throws ParseException {
        String submissionDateString = submission.getAttributes().getNamedItem(
                XML_DATE).getNodeValue();
        Date submissionDate = new SimpleDateFormat(
                XML_DATEFORMAT).parse(submissionDateString);
        String submissionTimeString = submission.getAttributes().getNamedItem(
                XML_TIME).getNodeValue();
        Date submissionTime = new SimpleDateFormat(
                XML_TIMEFORMAT).parse(submissionTimeString);
        GregorianCalendar time = new GregorianCalendar();
        time.setTime(submissionTime);
        GregorianCalendar date = new GregorianCalendar();
        date.setTime(submissionDate);
        GregorianCalendar calendar = new GregorianCalendar(
                date.get(Calendar.YEAR),
                date.get(Calendar.MONTH),
                date.get(Calendar.DAY_OF_MONTH),
                time.get(Calendar.HOUR_OF_DAY),
                time.get(Calendar.MINUTE),
                time.get(Calendar.SECOND));
        return calendar.getTime();
    }

    /**
     * Parse inputstream data into xml document instance
     * @param is provided inputstream
     * @return parsed xml document
     * @throws SAXException
     * @throws IOException
     * @throws ParserConfigurationException
     */
    private static Document parse(InputStream is) throws SAXException, IOException, ParserConfigurationException {
        Document ret = null;
        DocumentBuilderFactory domFactory;
        DocumentBuilder builder;
        domFactory = DocumentBuilderFactory.newInstance();
        domFactory.setValidating(false);
        domFactory.setNamespaceAware(false);
        builder = domFactory.newDocumentBuilder();
        ret = builder.parse(is);
        return ret;
    }

    private final static String XML_TAG__SUBMISSION = "submission";
    private static final String XML_DATE				= "date";
    private static final String XML_DATEFORMAT			= "yyyy-MM-dd";
    private static final String XML_TIME				= "time";
    private static final String XML_TIMEFORMAT			= "hh:mm:ss";
    private static final String XML_FIELD				= "field";
    private static final String XML_FIELD_NAME			= "name";
    private static final String XML_FIELD__NAME_DES_KINDES = "Name_des_Kindes";
    private static final String XML_FIELD__KLASSE = "Klasse";
    private static final String XML_FIELD__TAEGLICHE_BETREUUNG = "Taegliche_Betreuung";
    private static final String XML_FIELD__GTS_1_MONTAG = "GTS-1-Montag";
    private static final String XML_FIELD__GTS_1_DIENSTAG = "GTS-1-Dienstag";
    private static final String XML_FIELD__GTS_1_MITTWOCH = "GTS-1-Mittwoch";
    private static final String XML_FIELD__GTS_1_DONNERSTAG = "GTS-1-Donnerstag";
    private static final String XML_FIELD__GTS_1_FREITAG = "GTS-1-Freitag";
    private static final String XML_FIELD__GTS_2_MONTAG_WUNSCH_1 = "GTS-2_Montag_Wunsch-1";
    private static final String XML_FIELD__GTS_2_MONTAG_WUNSCH_2 = "GTS-2_Montag_Wunsch-2";
    private static final String XML_FIELD__GTS_2_MONTAG_WUNSCH_3 = "GTS-2_Montag_Wunsch-3";
    private static final String XML_FIELD__GTS_2_DIENSTAG_WUNSCH_1 = "GTS-2_Dienstag_Wunsch-1";
    private static final String XML_FIELD__GTS_2_DIENSTAG_WUNSCH_2 = "GTS-2_Dienstag_Wunsch-2";
    private static final String XML_FIELD__GTS_2_DIENSTAG_WUNSCH_3 = "GTS-2_Dienstag_Wunsch-3";
    private static final String XML_FIELD__GTS_2_MITTWOCH_WUNSCH_1 = "GTS-2_Mittwoch_Wunsch-1";
    private static final String XML_FIELD__GTS_2_MITTWOCH_WUNSCH_2 = "GTS-2_Mittwoch_Wunsch-2";
    private static final String XML_FIELD__GTS_2_MITTWOCH_WUNSCH_3 = "GTS-2_Mittwoch_Wunsch-3";
    private static final String XML_FIELD__GTS_2_DONNERSTAG_WUNSCH_1 = "GTS-2_Donnerstag_Wunsch-1";
    private static final String XML_FIELD__GTS_2_DONNERSTAG_WUNSCH_2 = "GTS-2_Donnerstag_Wunsch-2";
    private static final String XML_FIELD__GTS_2_DONNERSTAG_WUNSCH_3 = "GTS-2_Donnerstag_Wunsch-3";
    private static final String XML_FIELD__GTS_2_FREITAG_WUNSCH_1 = "GTS-2_Freitag_Wunsch-1";
    private static final String XML_FIELD__GTS_2_FREITAG_WUNSCH_2 = "GTS-2_Freitag_Wunsch-2";
    private static final String XML_FIELD__GTS_2_FREITAG_WUNSCH_3 = "GTS-2_Freitag_Wunsch-3";
    private static final String XML_FIELD__GTS_3_MONTAG = "GTS-3-Montag";
    private static final String XML_FIELD__GTS_3_DIENSTAG = "GTS-3-Dienstag";
    private static final String XML_FIELD__GTS_3_MITTWOCH = "GTS-3-Mittwoch";
    private static final String XML_FIELD__GTS_3_DONNERSTAG = "GTS-3-Donnerstag";
    private static final String XML_FIELD__GTS_3_FREITAG = "GTS-3-Freitag";
    private static final String XML_FIELD__ANTRAG_AUF_ERMAESSIGUNG = "Antrag_auf_Ermäßigung";
    private static final String XML_FIELD__ERMAESSIGUNGSGRUND_1 = "Ermäßigungsgrund-1";
    private static final String XML_FIELD__NAME_DES_GESCHWISTERKINDES = "Name_des_Geschwisterkindes";
    private static final String XML_FIELD__KLASSE_DES_GESCHWISTERKINDES = "Klasse_des_Geschwisterkindes";
    private static final String XML_FIELD__ERMAESSIGUNGSGRUND_2 = "Ermäßigungsgrund-2";
    private static final String XML_FIELD__ERMAESSIGUNGSGRUND_3 = "Ermäßigungsgrund-3";
    private static final String XML_FIELD__BEGRUENDUNG = "Begründung";
    private static final String XML_FIELD__ERZIEHUNGSBERECHTIGTER = "Erziehungsberechtigter";
    private static final String XML_FIELD__ORT = "Ort";
    private static final String XML_FIELD__TELEFONNUMMER = "Telefonnummer";
}
