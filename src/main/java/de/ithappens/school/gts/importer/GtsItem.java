package de.ithappens.school.gts.importer;

import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;

/**
 * Copyright: 2020
 * Organization: IT-Happens.de
 *
 * @author Ryppo
 */
public abstract class GtsItem {

    public enum GTS_LEVEL {GTS_1, GTS_2, GTS_3, NOT_SET}

    public enum DAY_OF_WEEK {MONDAY, TUESDAY, WEDNESDAY, THURSDAY, FRIDAY, NOT_SET}

    public enum PRIORITY {ONE, TWO, THREE, NOT_SET}

    private BigDecimal costsInEuro = BigDecimal.ZERO;
    private GTS_LEVEL gtsLevel = defineGtsLevel();
    private DAY_OF_WEEK dayOfWeek = DAY_OF_WEEK.NOT_SET;
    private PRIORITY priority = PRIORITY.NOT_SET;
    private String rawValue;

    public void setCostsInEuro(BigDecimal costsInEuro) {
        this.costsInEuro = costsInEuro;
    }

    public BigDecimal getCostsInEuro() {
        return costsInEuro;
    }

    public String getRawValue() {
        return rawValue;
    }

    public void setRawValue(String rawValue) {
        this.rawValue = rawValue;
    }

    public abstract GTS_LEVEL defineGtsLevel();

    public void setDayOfWeek(DAY_OF_WEEK dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public DAY_OF_WEEK getDayOfWeek() {
        return dayOfWeek;
    }

    public PRIORITY getPriority() {
        return priority;
    }

    public void setPriority(PRIORITY priority) {
        this.priority = priority;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof GtsItem) {
            GtsItem guineaPig = (GtsItem) obj;
            return gtsLevel.equals(guineaPig.gtsLevel)
                    && dayOfWeek.equals(guineaPig.dayOfWeek)
                    && priority.equals(guineaPig.priority);
        }
        return false;
    }
}
