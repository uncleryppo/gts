package de.ithappens.school.gts.importer;

/**
 * Copyright: 2020
 * Organization: IT-Happens.de
 *
 * @author Ryppo
 */
public class ErmaessigungsGrund3Item extends ErmaessigungItem {

    private String begruendung;

    @Override
    public NUMBER defineNumber() {
        return NUMBER.THREE;
    }

    public String getBegruendung() {
        return begruendung;
    }

    public void setBegruendung(String begruendung) {
        this.begruendung = begruendung;
    }
}
