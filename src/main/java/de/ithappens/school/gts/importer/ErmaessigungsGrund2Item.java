package de.ithappens.school.gts.importer;

/**
 * Copyright: 2020
 * Organization: IT-Happens.de
 *
 * @author Ryppo
 */
public class ErmaessigungsGrund2Item extends ErmaessigungItem {

    @Override
    public NUMBER defineNumber() {
        return NUMBER.TWO;
    }
}
