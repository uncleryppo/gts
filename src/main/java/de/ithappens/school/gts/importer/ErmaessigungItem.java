package de.ithappens.school.gts.importer;

/**
 * Copyright: 2020
 * Organization: IT-Happens.de
 *
 * @author Ryppo
 */
public abstract class ErmaessigungItem {

    private NUMBER number = defineNumber();
    private String rawValue;

    public NUMBER getNumber() {
        return number;
    }

    public void setNumber(NUMBER number) {
        this.number = number;
    }

    public String getRawValue() {
        return rawValue;
    }

    public void setRawValue(String rawValue) {
        this.rawValue = rawValue;
    }

    public enum NUMBER {ONE, TWO, THREE, NOT_SET}

    public abstract NUMBER defineNumber();

}
