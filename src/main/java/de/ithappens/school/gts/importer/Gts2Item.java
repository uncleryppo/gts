package de.ithappens.school.gts.importer;

import java.math.BigDecimal;

/**
 * Copyright: 2020
 * Organization: IT-Happens.de
 *
 * @author Ryppo
 */
public class Gts2Item extends GtsItem {

    public Gts2Item(DAY_OF_WEEK dayOfWeek, PRIORITY priority, String value, BigDecimal costsInEuro) {
        setDayOfWeek(dayOfWeek);
        setPriority(priority);
        setRawValue(value);
        setCostsInEuro(costsInEuro);
    }

    @Override
    public GTS_LEVEL defineGtsLevel() {
        return GTS_LEVEL.GTS_2;
    }
}
