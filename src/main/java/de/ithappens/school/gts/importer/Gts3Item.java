package de.ithappens.school.gts.importer;

/**
 * Copyright: 2020
 * Organization: IT-Happens.de
 *
 * @author Ryppo
 */
public class Gts3Item extends GtsItem {

    public Gts3Item(DAY_OF_WEEK dayOfWeek, String rawValue) {
        setDayOfWeek(dayOfWeek);
        setRawValue(rawValue);
    }

    @Override
    public GTS_LEVEL defineGtsLevel() {
        return GTS_LEVEL.GTS_3;
    }
}
