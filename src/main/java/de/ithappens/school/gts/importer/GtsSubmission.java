package de.ithappens.school.gts.importer;

import org.apache.commons.lang3.StringUtils;

import java.util.*;

/**
 * Copyright: 2020
 * Organization: IT-Happens.de
 * @author Ryppo
 */
public class GtsSubmission {

    private Date creationDate = new Date();
    private String nameDesKindes, klasse, taeglicheBetreuung, antragAufErmaessigung,
            erziehungsberechtigter, ort, telefonnummer;

    public final List<Gts1Item> gts1Items = new ArrayList<>();
    public final List<Gts2Item> gts2Items = new ArrayList<>();
    public final List<Gts3Item> gts3Items = new ArrayList<>();

    private final ErmaessigungsGrund1Item ermaessigungsGrund1 = new ErmaessigungsGrund1Item();
    private final ErmaessigungsGrund2Item ermaessigungsGrund2 = new ErmaessigungsGrund2Item();
    private final ErmaessigungsGrund3Item ermaessigungsGrund3 = new ErmaessigungsGrund3Item();

    public final Map<String, String> unknownAttributes = new HashMap<>();

    /**
     * Create and get the identification as string
     * @return identification
     */
    public String identify() {
        return StringUtils.join(new String[]{nameDesKindes, klasse}, " | ") + " | Unknown attributes count: " + unknownAttributes.size();
    }

    /**
     * Get the creation date value
     * @return creation date value
     */
    public Date getCreationDate() {
        return creationDate;
    }

    /**
     * Set the creation date value
     * @param creationDate creation date value
     */
    public void setCreationDate(Date creationDate) {
        this.creationDate = creationDate;
    }

    /**
     * Get the Klasse value
     * @return Klasse value
     */
    public String getKlasse() {
        return klasse;
    }

    /**
     * Set the Klasse value
     * @param klasse Klasse value
     */
    public void setKlasse(String klasse) {
        this.klasse = klasse;
    }

    /**
     * Get the Name des Kindes value
     * @return Name des Kindes value
     */
    public String getNameDesKindes() {
        return nameDesKindes;
    }

    /**
     * Set the Name des Kindes value
     * @param nameDesKindes Name des Kindes value
     */
    public void setNameDesKindes(String nameDesKindes) {
        this.nameDesKindes = nameDesKindes;
    }

    /**
     * Get the Tägliche Betreuung value
     * @return Tägliche Betreuung value
     */
    public String getTaeglicheBetreuung() {
        return taeglicheBetreuung;
    }

    /**
     * Set the Tägliche Betreuung value
     * @param taeglicheBetreuung Tägliche Betreuung value
     */
    public void setTaeglicheBetreuung(String taeglicheBetreuung) {
        this.taeglicheBetreuung = taeglicheBetreuung;
    }

    /**
     * Get the Antrag auf Ermäßigung value
     * @return Antrag auf Ermäßigung value
     */
    public String getAntragAufErmaessigung() {
        return antragAufErmaessigung;
    }

    /**
     * Set the Antrag auf Ermäßigung value
     * @param antragAufErmaessigung Antrag auf Ermäßigung value
     */
    public void setAntragAufErmaessigung(String antragAufErmaessigung) {
        this.antragAufErmaessigung = antragAufErmaessigung;
    }

    /**
     * Get the Ermäßigungsgrund 1 value
     * @return Ermäßigungsgrund 1 value
     */
    public ErmaessigungsGrund1Item getErmaessigungsGrund1() {
        return ermaessigungsGrund1;
    }

    /**
     * Get the Ermäßigungsgrund 2 value
     * @return Ermäßigungsgrund 2 value
     */
    public ErmaessigungsGrund2Item getErmaessigungsGrund2() {
        return ermaessigungsGrund2;
    }

    /**
     * Get the Ermäßigungsgrund 3 value
     * @return Ermäßigungsgrund 3 value
     */
    public ErmaessigungsGrund3Item getErmaessigungsGrund3() {
        return ermaessigungsGrund3;
    }

    /**
     * Get the Erziehungsberechtigter value
     * @return Erziehungsberechtigter value
     */
    public String getErziehungsberechtigter() {
        return erziehungsberechtigter;
    }

    /**
     * Set the Erziehungsberechtigter value
     * @param erziehungsberechtigter Erziehungsberechtigter value
     */
    public void setErziehungsberechtigter(String erziehungsberechtigter) {
        this.erziehungsberechtigter = erziehungsberechtigter;
    }

    /**
     * Get the Ort value
     * @return Ort value
     */
    public String getOrt() {
        return ort;
    }

    /**
     * Set the Ort value
     * @param ort Ort value
     */
    public void setOrt(String ort) {
        this.ort = ort;
    }

    /**
     * Get the Telefonnummer value
     * @return Telefonnummer value
     */
    public String getTelefonnummer() {
        return telefonnummer;
    }

    /**
     * Set the Telefonnummer value
     * @param telefonnummer Telefonnummer value
     */
    public void setTelefonnummer(String telefonnummer) {
        this.telefonnummer = telefonnummer;
    }
}
