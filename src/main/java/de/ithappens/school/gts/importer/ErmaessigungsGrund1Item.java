package de.ithappens.school.gts.importer;

/**
 * Copyright: 2020
 * Organization: IT-Happens.de
 *
 * @author Ryppo
 */
public class ErmaessigungsGrund1Item extends ErmaessigungItem {

    private String nameDesGeschwisterKindes, klasseDesGeschwisterKindes;

    public ErmaessigungsGrund1Item() {}

    public ErmaessigungsGrund1Item(String rawValue, String nameDesGeschwisterKindes, String klasseDesGeschwisterKindes) {
        setRawValue(rawValue);
        this.nameDesGeschwisterKindes = nameDesGeschwisterKindes;
        this.klasseDesGeschwisterKindes = klasseDesGeschwisterKindes;
    }

    /**
     * Get the Name des Geschwisterkindes value
     * @return Name des Geschwisterkindes value
     */
    public String getNameDesGeschwisterKindes() {
        return nameDesGeschwisterKindes;
    }

    /**
     * Set the Name des Geschwisterkindes value
     * @param nameDesGeschwisterKindes Name des Geschwisterkindes value
     */
    public void setNameDesGeschwisterKindes(String nameDesGeschwisterKindes) {
        this.nameDesGeschwisterKindes = nameDesGeschwisterKindes;
    }

    /**
     * Get the Klasse des Geschwisterkindes value
     * @return Klasse des Geschwisterkindes value
     */
    public String getKlasseDesGeschwisterKindes() {
        return klasseDesGeschwisterKindes;
    }

    /**
     * Set the Klasse des Geschwisterkindes value
     * @param klasseDesGeschwisterKindes Klasse des Geschwisterkindes value
     */
    public void setKlasseDesGeschwisterKindes(String klasseDesGeschwisterKindes) {
        this.klasseDesGeschwisterKindes = klasseDesGeschwisterKindes;
    }

    @Override
    public NUMBER defineNumber() {
        return NUMBER.ONE;
    }
}
