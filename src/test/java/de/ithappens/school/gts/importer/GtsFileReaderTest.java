package de.ithappens.school.gts.importer;

import org.junit.Test;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;

/**
 * Copyright: 2020
 * Organization: IT-Happens.de
 * @author Ryppo
 */
public class GtsFileReaderTest {

    private final URL gtsUrl = new URL("http://grundschule-am-see.de/gts-anmeldungen-2020-1hj.xml");

    public GtsFileReaderTest() throws MalformedURLException {
    }

    @Test
    public void printGtsFile_empty() throws ParserConfigurationException, SAXException, IOException, ParseException {
        GtsFileReader.printGtsFile(null);
    }

    @Test
    public void printGtsFile_gsas() throws ParserConfigurationException, SAXException, IOException, ParseException {
        GtsFileReader.printGtsFile(gtsUrl);
    }
}